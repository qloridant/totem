<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xenc="http://www.w3.org/2001/04/xmlenc#" xmlns:xad="http://uri.etsi.org/01903/v1.1.1#" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.minefi.gouv.fr/cp/demat/docbudgetaire" targetNamespace="http://www.minefi.gouv.fr/cp/demat/docbudgetaire" elementFormDefault="qualified" xmlns:z="odm:enrichissement">
	<xs:include schemaLocation="Class_LigneBudget.xsd"/>
	<xs:include schemaLocation="Class_PJReference.xsd"/>
	<xs:include schemaLocation="CommunBudget.xsd"/>
	<xs:include schemaLocation="CommunAnnexe.xsd"/>
	<xs:include schemaLocation="Simple_Type.xsd"/>
	<xs:include schemaLocation="Class_Annexes.xsd"/>
	<xs:complexType name="TBudget">
		<xs:sequence>
			<xs:element name="EnTeteBudget" type="TEnTeteBudget"/>
			<xs:element name="BlocBudget" type="TBlocBudget"/>
			<xs:element name="InformationsGenerales" type="TInformationsGenerales">
				<xs:annotation>
					<xs:documentation>Informations statistiques, fiscales et financières</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="LigneBudget" type="TLigneBudget" maxOccurs="unbounded"/>
			<xs:element name="Annexes" type="TAnnexes" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="TEnTeteBudget">
		<xs:sequence>
			<xs:element name="LibelleEtab" type="Base_Texte100">
				<xs:annotation>
					<xs:documentation>Libellé du Budget Collectivité</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="IdEtab" type="Base_Siret" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Identifiant de l'établissement</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="CodColl" type="Base_Alphanum3">
				<xs:annotation>
					<xs:documentation>Code collectivité. : identification de la collectivité ou du budget collectivité</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="CodBud" type="Base_Alphanum2">
				<xs:annotation>
					<xs:documentation>Code budget. S'il s'agit du budget principal ou si le code collectivité sert déjà à identifier le budget collectivité, cette rubrique est mise à 00.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Nomenclature" type="TNomenclature">
				<xs:annotation>
					<xs:documentation>Définition de la nomenclature de la collectivité.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="TBlocBudget">
		<xs:sequence>
			<xs:element name="Affect" type="Base_Texte10" minOccurs="0">
				<xs:annotation>
					<xs:documentation>(PES) Critère d'affectation : Zone libre permettant  à l’ordonnateur de déterminer l’affectation du dossier dans les services du comptable. Les critères d'affectation résultent d’une convention entre l'ordonnateur et le comptable.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="NatDec" type="TNatDec">
				<xs:annotation>
					<xs:documentation>Nature de la décision.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="NumDec" type="Base_Alphanum10" minOccurs="0">
				<xs:annotation>
					<xs:documentation>N° de la décision (dans le cas d'une DM)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Exer" type="Base_Annee">
				<xs:annotation>
					<xs:documentation>Millésime de l'exercice budgétaire concerné</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="PJRef" type="TPJReference" minOccurs="0" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>Référence à une pièce justificative</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="DteDec" type="Base_Date">
				<xs:annotation>
					<xs:documentation>(PES) Date de la décision</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="DteDecEx" type="Base_Date">
				<xs:annotation>
					<xs:documentation>(PES) Date du caractère exécutoire de la décision</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="NatVote" type="TNatVote" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Nature de la vote du budget pour indiquer si le vote est au niveau du chapitre ou de l'article pour la section de fonctionnement ; si le vote est au niveau du chapitre ou de l'article pour la section d'investissement.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="OpeEquip" type="Base_Booleen" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Prise en compte des opérations d'équipement</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="VoteFormelChap" type="Base_Booleen" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Vote formel sur chacun des chapitres d'équipement</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="TypProv" type="TTypProv" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Type de provisions</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="RefProv" type="Base_Texte50" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Référence délibéré relatif aux provisions</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="BudgPrec" type="TBudgPrec">
				<xs:annotation>
					<xs:documentation>Budget précédent</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="ReprRes" type="TReprRes">
				<xs:annotation>
					<xs:documentation>Budget avec ou sans reprise des résultats N-1</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="NatFonc" type="TNatFonc">
				<xs:annotation>
					<xs:documentation>Indication si le budget est voté par nature, fonction ou mixte</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="DepFoncN2" type="Base_Montant" minOccurs="0">
				<xs:annotation>
					<xs:documentation>002 Résultat reporté N-2 en dépense</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="RecFoncN2" type="Base_Montant" minOccurs="0">
				<xs:annotation>
					<xs:documentation>002 Résultat reporté N-2 en recette</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="DepInvN2" type="Base_Montant" minOccurs="0">
				<xs:annotation>
					<xs:documentation>001 Solde d'investissement N-2 en dépense</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="RecInvN2" type="Base_Montant" minOccurs="0">
				<xs:annotation>
					<xs:documentation>001 Solde d'investissement N-2 en recette</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="CodTypBud" type="ATCodTypBud">
				<xs:annotation>
					<xs:documentation>Type budget : Principal ou Annexe</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="IdEtabPal" type="Base_Num14" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Identifiant de l'établissement principal</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="TInformationsGenerales">
		<xs:sequence>
			<xs:element name="Information" type="TInformationGenerale" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
</xs:schema>
